package agent

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/cilium/cilium/api/v1/flow"
	"github.com/cilium/cilium/api/v1/observer"
	v2 "github.com/cilium/cilium/pkg/k8s/apis/cilium.io/v2"
	"github.com/cilium/cilium/pkg/k8s/client/clientset/versioned"
	informers_v2 "github.com/cilium/cilium/pkg/k8s/client/informers/externalversions/cilium.io/v2"
	monitor_api "github.com/cilium/cilium/pkg/monitor/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/tool/errz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/tool/retry"
	"go.uber.org/zap"
	"google.golang.org/protobuf/types/known/timestamppb"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/tools/cache"
)

const (
	informerNotifierIndex = "InformerNotifierIdx"
	gitLabProjectLabel    = "app.gitlab.com/proj"
)

type worker struct {
	log                  *zap.Logger
	api                  modagent.Api
	ciliumClient         versioned.Interface
	observerClient       observer.ObserverClient
	pollConfig           retry.PollConfigFactory
	informerResyncPeriod time.Duration
	projectId            int64
}

func cnpIndexFunc(obj interface{}) ([]string, error) {
	cnp := obj.(*v2.CiliumNetworkPolicy)

	if alertsEnabled := cnp.Annotations[alertAnnotationKey]; alertsEnabled != "true" {
		return nil, nil
	}

	projectId, ok := cnp.Labels[gitLabProjectLabel]
	if !ok {
		return nil, nil
	}

	return []string{projectId}, nil
}

func (w *worker) Run(ctx context.Context) {
	labelSelector := metav1.FormatLabelSelector(&metav1.LabelSelector{
		MatchExpressions: []metav1.LabelSelectorRequirement{
			{
				Key:      gitLabProjectLabel,
				Operator: metav1.LabelSelectorOpExists,
			},
		},
	})

	ciliumEndpointInformer := informers_v2.NewFilteredCiliumNetworkPolicyInformer(
		w.ciliumClient,
		metav1.NamespaceNone,
		w.informerResyncPeriod,
		cache.Indexers{informerNotifierIndex: cnpIndexFunc},
		func(listOptions *metav1.ListOptions) { listOptions.LabelSelector = labelSelector },
	)

	var wg wait.Group
	defer wg.Wait()
	wg.StartWithChannel(ctx.Done(), ciliumEndpointInformer.Run)

	if !cache.WaitForCacheSync(ctx.Done(), ciliumEndpointInformer.HasSynced) {
		return
	}

	since := timestamppb.Now()
	_ = retry.PollWithBackoff(ctx, w.pollConfig(), func() (error, retry.AttemptResult) {
		ctx, cancel := context.WithCancel(ctx) // nolint:govet
		defer cancel()
		flc, err := w.observerClient.GetFlows(ctx, &observer.GetFlowsRequest{
			Follow: true,
			Whitelist: []*flow.FlowFilter{
				{
					Verdict: []flow.Verdict{flow.Verdict_DROPPED}, //Drop verdicts only
					EventType: []*flow.EventTypeFilter{
						{
							Type: monitor_api.MessageTypePolicyVerdict, //L3_L4
						},
						{
							Type: monitor_api.MessageTypeAccessLog, //L7
						},
					},
				},
			},
			Since: since,
		})
		if err != nil {
			if !grpctool.RequestCanceledOrTimedOut(err) {
				w.log.Error("Failed to get flows from Hubble relay", logz.Error(err))
			}
			return nil, retry.Backoff
		}
		for {
			resp, err := flc.Recv()
			if err != nil {
				if errors.Is(err, io.EOF) {
					return nil, retry.Continue
				}
				if !grpctool.RequestCanceledOrTimedOut(err) {
					w.log.Error("GetFlows.Recv() failed", logz.Error(err))
				}
				return nil, retry.Backoff
			}
			// Remember when we got an event last time to resume from this timestamp
			since = timestamppb.Now()
			switch value := resp.ResponseTypes.(type) {
			case *observer.GetFlowsResponse_Flow:
				err = w.processFlow(ctx, value.Flow, ciliumEndpointInformer)
				if err != nil {
					w.log.Error("Flow processing failed", logz.Error(err))
					// continue consuming response messages
				}
			}
		}
	})
}

func (w *worker) processFlow(ctx context.Context, flw *flow.Flow, informer cache.SharedIndexInformer) error {
	cnps, err := informer.GetIndexer().ByIndex(informerNotifierIndex, strconv.FormatInt(w.projectId, 10))
	if err != nil {
		return err
	}

	cnp, err := getPolicy(flw, cnps)
	if err != nil {
		return err
	}
	if cnp == nil {
		return nil
	}
	return w.sendAlert(ctx, flw, cnp)
}

func (w *worker) sendAlert(ctx context.Context, fl *flow.Flow, cnp *v2.CiliumNetworkPolicy) (retErr error) {
	resp, err := w.api.MakeGitLabRequest(ctx, "/",
		modagent.WithRequestMethod(http.MethodPost),
		modagent.WithJsonRequestBody(&payload{
			Alert: alert{
				Flow:                prototool.JsonBox{Message: fl},
				CiliumNetworkPolicy: cnp,
			},
		}),
	)
	if err != nil {
		return fmt.Errorf("failed request to internal api: %w", err)
	}
	defer errz.SafeDrainAndClose(resp.Body, &retErr)
	switch resp.StatusCode {
	case http.StatusOK, http.StatusNoContent:
		w.log.Info("successful response when creating alerts from cilium_alert endpoint", zap.Int32("status_code", resp.StatusCode))
	default:
		return fmt.Errorf("failed to send flow to internal API: got %d HTTP response code", resp.StatusCode)
	}
	return nil
}

type payload struct {
	Alert alert `json:"alert"`
}

type alert struct {
	Flow                prototool.JsonBox       `json:"flow"`
	CiliumNetworkPolicy *v2.CiliumNetworkPolicy `json:"ciliumNetworkPolicy"`
}
